package com.example.helper

import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.FragmentTransaction
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SignInFTest {

    @get:Rule
    val testActivity = ActivityTestRule<MainActivity>(MainActivity::class.java)

    private var fragCommit : FragmentTransaction? = null

    @Before
    fun setUp() {
        fragCommit = testActivity.activity.supportFragmentManager.beginTransaction()
    }

    @Test
    fun testLaunch() {
        val inflater = testActivity.activity.layoutInflater
        val root = inflater.inflate(R.layout.fragment_sign_in, null, false)

        val login = root.findViewById<EditText>(R.id.login)
        val password = root.findViewById<EditText>(R.id.password)
        val signIn = root.findViewById<Button>(R.id.signinBtn)
        Espresso.onView(withId(R.id.startTest)).perform(click())
        Thread.sleep(4000)
        Espresso.onView(withId(login.id)).perform(clearText())
        Espresso.onView(withId(login.id)).perform(replaceText("trrr@gmail.ru"))
        Thread.sleep(1000)
        Espresso.onView(withId(password.id)).perform(clearText())
        Espresso.onView(withId(password.id)).perform(replaceText("password"))
        Thread.sleep(1000)
        Espresso.onView(withId(signIn.id)).perform(click())
        Thread.sleep(2000)
        Espresso.onView(withId(R.id.fioTxt)).check(ViewAssertions.matches(withText("Vladislav Snowden")));
    }

    @After
    fun tearDown(){
        fragCommit = null
    }

}