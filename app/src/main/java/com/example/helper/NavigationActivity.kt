package com.example.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.example.helper.api.RetrofitClient
import com.example.helper.model.UpdateAvatar
import com.example.helper.tutorials.NavigationViewTutorial
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.fragment_map.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class NavigationActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private var mySettings: SharedPreferences? = null

    private lateinit var navController: NavController

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        NavigationUI.onNavDestinationSelected(p0, navController)
        drawer_layout.closeDrawer(GravityCompat.START)
        return super.onOptionsItemSelected(p0)
    }

    private fun onCreateTutorials(alertAnswer: Boolean) {

        if (NavigationViewTutorial.getTutorialStatus(this)) {

            Handler().postDelayed(Runnable {
                val drawerFio = view().findViewById<TextView>(R.id.fioTxt)
                val close = view().findViewById<Button>(R.id.closeDrawer)


                TapTargetSequence(this@NavigationActivity)
                    .targets(
                        TapTarget.forView(
                            drawerFio, getString(R.string.profileTT),
                            getString(R.string.profileTTD)
                        ).cancelable(false).transparentTarget(true).targetRadius(70).id(0)
                            .outerCircleColor(R.color.colorCET)
                            .titleTextColor(R.color.headerBG)
                            .textColor(R.color.menuBG),
                        TapTarget.forView(
                            web, getString(R.string.webTT),
                            getString(R.string.webTTD)
                        ).cancelable(false).transparentTarget(true).targetRadius(70).id(1)
                            .outerCircleColor(R.color.colorCET)
                            .titleTextColor(R.color.headerBG)
                            .textColor(R.color.menuBG),
                        TapTarget.forView(
                            close,
                            getString(R.string.menuCloseTT),
                            getString(R.string.menuCloseTTD)
                        )
                            .cancelable(false).transparentTarget(true).targetRadius(70).id(2)
                            .outerCircleColor(R.color.colorCET)
                            .titleTextColor(R.color.headerBG)
                            .textColor(R.color.menuBG),
                        TapTarget.forView(
                            openDrawer,
                            getString(R.string.menuOpenTT),
                            getString(R.string.menuOpenTTD)
                        )
                            .cancelable(false).transparentTarget(true).targetRadius(70).id(3)
                            .outerCircleColor(R.color.colorCET)
                            .titleTextColor(R.color.headerBG)
                            .textColor(R.color.menuBG)
                    ).listener(object : TapTargetSequence.Listener {
                        override fun onSequenceCanceled(lastTarget: TapTarget?) {
                            return
                        }

                        override fun onSequenceFinish() {
                            Toast.makeText(
                                this@NavigationActivity,
                                getString(R.string.toastHelper),
                                Toast.LENGTH_LONG
                            ).show()
                        }

                        override fun onSequenceStep(
                            lastTarget: TapTarget?,
                            targetClicked: Boolean
                        ) {
                            when (lastTarget!!.id()) {
                                2 -> {
                                    drawer_layout.closeDrawer(GravityCompat.START)
                                }
                            }
                        }
                    })
                    .start()
            }, 1000)

        }

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        updateFio()
        updateAvatar()
        onCreateTutorials(true)
    }

    @SuppressLint("MissingPermission", "RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mySettings = getSharedPreferences("Lang", Context.MODE_PRIVATE)
        val lang = mySettings!!.getString("lang", "ru")
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val conf = Configuration()
        conf.setLocale(locale)
        resources.updateConfiguration(conf, resources.displayMetrics)
        setContentView(R.layout.activity_navigation)
        drawer_layout.setScrimColor(Color.TRANSPARENT)
        drawer_layout.openDrawer(GravityCompat.START)

        navController = findNavController(R.id.nav_host_fragment)

        val navView = findViewById<NavigationView>(R.id.nav_view)

        web.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.data = Uri.parse("https://vk.com/lordanti")
            startActivity(browserIntent)
        }

        nav_view.setupWithNavController(navController)
        navView.setNavigationItemSelectedListener(this)
        val actionBarDrawerToggle =
            object : ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close) {

                @SuppressLint("RestrictedApi")
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    super.onDrawerSlide(drawerView, slideOffset)

                    val slideX = (nav_view.width) * slideOffset
                    content.translationX = slideX
                    if (openDrawer != null) {
                        if (content.translationX.toString() == "0.0") {
                            openDrawer.visibility =
                                View.VISIBLE
                        } else openDrawer.visibility = View.INVISIBLE
                    }
                }
            }

        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        drawer_layout.drawerElevation = 0f

        val drawerClose = view().findViewById<Button>(R.id.closeDrawer)

        val mNavigationView = this.findViewById<LinearLayout>(R.id.menu)
        val params = mNavigationView.layoutParams
        params.width = resources.displayMetrics.widthPixels
        mNavigationView.layoutParams = params

        drawerClose.setOnClickListener {
            drawer_layout.closeDrawers()
        }
    }

    private fun updateAvatar() {
        val token = intent.getStringExtra("token")
        val image = view().findViewById<ImageView>(R.id.imageView)

        RetrofitClient.instance.getAvatar(token).enqueue(object : Callback<UpdateAvatar> {
            override fun onFailure(call: Call<UpdateAvatar>, t: Throwable) {
                Toast.makeText(
                    this@NavigationActivity,
                    t.message,
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(call: Call<UpdateAvatar>, response: Response<UpdateAvatar>) {
                if (response.isSuccessful) {
                    image.setImageBitmap(BitmapFactory.decodeFile(response.body()!!.avatar))
                } else {
                    Toast.makeText(
                        this@NavigationActivity,
                        "Avatar",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    private fun updateFio() {
        val drawerFio = view().findViewById<TextView>(R.id.fioTxt)
        val token = intent.getStringExtra("token")

        RetrofitClient.instance.getFio(token).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                drawerFio.text = "F.I.O"
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                if (response.isSuccessful) {
                    drawerFio.text = response.body()!!.string()
                } else {
                    Toast.makeText(
                        this@NavigationActivity,
                        "Something wrong with FIO!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    private fun view(): View {
        val nv = this.findViewById<NavigationView>(R.id.nav_view_header)
        return nv.getHeaderView(0)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}
