package com.example.helper.api

import com.example.helper.model.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface Api {

    @POST("login")
    fun userLogin(@Body user: User): Call<LoginResponse>

    @GET("profile")
    fun getFio(@Header("Authorization") authToken: String): Call<ResponseBody>

    @POST("signup")
    fun createUser(@Body registration: Registration): Call<ResponseMessage>

    @GET("cars")
    fun getCars(): Call<ArrayList<Car>>

    @POST("newbook")
    fun createBook(@Header("Authorization") authToken: String, @Body book: Book): Call<ResponseBody>

    @POST("logout")
    fun userLogout(@Header("Authorization") authToken: String): Call<ResponseBody>

    @GET("allprofileinfo")
    fun getUserAllInfo(@Header("Authorization") authToken: String): Call<Registration>

    @GET("token")
    fun getUserToken(@Header("Authorization") authToken: String) : Call<ResponseBody>

    @POST("emailupdate")
    fun updateEmail(@Header("Authorization") authToken: String, @Body updateEmail: UpdateEmail): Call<ResponseBody>

    @POST("fioupdate")
    fun updateName(@Header("Authorization") authToken: String, @Body updateName: UpdateName): Call<ResponseBody>

    @POST("passwordupdate")
    fun updatePassword(@Header("Authorization") authToken: String, @Body updatePassword: UpdatePassword): Call<ResponseBody>

    @POST("avatarupdate")
    fun updateAvatar(@Header("Authorization") authToken: String, @Body updateAvatar: UpdateAvatar): Call<ResponseBody>

    @GET("avatar")
    fun getAvatar(@Header("Authorization") authToken: String): Call<UpdateAvatar>

    @GET("books")
    fun getAllBooks(@Header("Authorization") authToken: String): Call<ArrayList<Books>>

    @POST("removebook")
    fun removeBook(@Body removeBook: RemoveBook): Call<ResponseBody>

    @POST("removeallbook")
    fun removeAllBook(@Header("Authorization") authToken: String): Call<ResponseBody>

    @GET("favorites")
    fun getAllFavorites(@Header("Authorization") authToken: String): Call<ArrayList<Favorites>>

    @POST("removeallfavorites")
    fun removeAllFavorites(@Header("Authorization") authToken: String): Call<ResponseBody>

    @POST("removefavorites")
    fun removeFavorites(@Body removeFavorites: RemoveFavorites): Call<ResponseBody>

    @POST("newfavorites")
    fun createFavorites(@Header("Authorization") authToken: String, @Body favorite: Favorite): Call<ResponseBody>

    @POST("carinfo")
    fun getCarInfo(@Header("Authorization") authToken: String, @Body carId: CarId): Call<Car>
}