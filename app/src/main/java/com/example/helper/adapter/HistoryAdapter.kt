package com.example.helper.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.helper.R
import com.example.helper.api.RetrofitClient
import com.example.helper.model.Book
import com.example.helper.model.Books
import com.example.helper.model.RemoveBook
import kotlinx.android.synthetic.main.item_history.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryAdapter(private val items: ArrayList<Books>, token : String) :
    RecyclerView.Adapter<HistoryAdapter.VH>() {

    private val token = token

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(parent)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun restoreItem(model: Books, position: Int) {
        val book = Book(
            model.car,
            model.time_wait,
            model.time_drive,
            model.wait_price,
            model.drive_price
        )
        RetrofitClient.instance.createBook(token, book).enqueue(object  : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("Error restore!", t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){
                    items.add(position, model)
                    notifyItemInserted(position)
                }else{
                    Log.e("Bad response", response.body().toString())
                }
            }

        })
    }

    fun removeItem(position: Int) {
        val removeBook = RemoveBook(items[position]._id)
        RetrofitClient.instance.removeBook(removeBook).enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("FAIL", t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){
                    items.removeAt(position)
                    notifyItemRemoved(position)
                    notifyItemRangeChanged(position, items.size)
                }else{
                    Log.e("WRONG!", response.body().toString())
                }
            }

        })
    }

    class VH(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
    ) {

        fun bind(name: Books) {
            with(itemView) {
                valueCar.text = name.car
                valueDrivePrice.text = name.drive_price
                valueTimeDrive.text = name.time_drive
                valueTimeWait.text = name.time_wait
                valueWaitPrice.text = name.wait_price
            }
        }
    }
}