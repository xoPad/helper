package com.example.helper.adapter

import android.app.AlertDialog
import android.content.Context
import android.location.Geocoder
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.helper.R
import com.example.helper.api.RetrofitClient
import com.example.helper.model.Car
import com.example.helper.model.Favorite
import kotlinx.android.synthetic.main.alert_name_favorites.view.*
import kotlinx.android.synthetic.main.item_bsfavorites.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class BSFAdapter(private val items: ArrayList<Car>, token: String, context: Context) :
    RecyclerView.Adapter<BSFAdapter.VH>() {

    private val token = token

    private val context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(parent)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
        val view = holder.itemView
        holder.itemView.setOnClickListener{
            val alert = AlertDialog.Builder(view.context)
            val inf = LayoutInflater.from(view.context)
            val dialogView = inf.inflate(R.layout.alert_name_favorites, null)
            alert.setView(dialogView)
            alert.setTitle(context.getString(R.string.alertTitleBS))
                .setPositiveButton("OK") { dialog, btn ->

                    if (dialogView.name.text.isEmpty()) {
                        dialogView.name.error = view.context.resources.getString(R.string.error_name)
                        dialogView.name.requestFocus()
                        return@setPositiveButton
                    } else {
                        val fav = Favorite(
                            items[holder.adapterPosition]._id,
                            items[holder.adapterPosition].mark,
                            dialogView.name.text.toString()
                        )

                        RetrofitClient.instance.createFavorites(token, fav)
                            .enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    Toast.makeText(view.context, t.message, Toast.LENGTH_LONG).show()
                                }

                                override fun onResponse(
                                    call: Call<ResponseBody>,
                                    response: Response<ResponseBody>
                                ) {
                                    if (response.isSuccessful) {
                                        Toast.makeText(view.context, "Item added", Toast.LENGTH_LONG).show()
                                    } else {
                                        Toast.makeText(view.context, response.message(), Toast.LENGTH_LONG).show()
                                    }
                                }
                            })
                    }


                }
                .setNegativeButton("Cancel") { dialogInterface, _ -> dialogInterface.cancel() }
            alert.create().show()
        }
    }

    class VH(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_bsfavorites, parent, false)
    ) {

        fun bind(car: Car) {
            with(itemView) {
                markCarValue.text = car.mark
                val geo = Geocoder(itemView.context, Locale.getDefault())
                val addressVal = geo.getFromLocation(
                    car.longitude.toDouble(),
                    car.latitude.toDouble(),
                    1
                )
                addressValue.text = addressVal[0].getAddressLine(0)
            }
        }

    }
}