package com.example.carsharing.tutorials

import android.content.Context

object NavigationViewTutorial {

    fun storeTutorialStatus(context: Context, show: Boolean) {
        val preferences = context.getSharedPreferences("showTutorial", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putBoolean("showNavigationTutorial", show)
        editor.apply()
    }

    fun getTutorialStatus(context: Context): Boolean {
        val preferences = context.getSharedPreferences("showTutorial", Context.MODE_PRIVATE)
        return preferences.getBoolean("showNavigationTutorial", true)
    }

}
