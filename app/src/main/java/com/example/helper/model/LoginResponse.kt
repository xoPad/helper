package com.example.helper.model

data class LoginResponse (
    val accessToken:String,
    val refreshToken:String
)