package com.example.helper.model

data class UpdatePassword(val password: String)