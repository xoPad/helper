package com.example.helper.model

data class UpdateEmail(val email: String)