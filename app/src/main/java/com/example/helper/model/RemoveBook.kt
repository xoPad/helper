package com.example.helper.model

data class RemoveBook(
    val _id: String
)