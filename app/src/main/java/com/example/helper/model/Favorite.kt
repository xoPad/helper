package com.example.carsharing.model

data class Favorite(
    val car_id:String,
    val car_name:String,
    val name:String
)