package com.example.helper.model

data class ResponseMessage(
    val message: String
)