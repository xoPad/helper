package com.example.helper.model

data class Car (
    val _id: String,
    val longitude: String,
    val latitude: String,
    val mark: String
)