package com.example.carsharing


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.carsharing.api.RetrofitClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class StartF : Fragment() {

    private lateinit var navController: NavController

    private var mySettings: SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        mySettings = requireActivity().getSharedPreferences("Account", Context.MODE_PRIVATE)
        val accessToken = mySettings!!.getString("TokenAccess", "")

        RetrofitClient.instance.getUserToken(accessToken!!)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val myIntent = Intent(activity, NavigationActivity::class.java)
                        myIntent.putExtra("token", accessToken)
                        activity!!.startActivity(myIntent)
                    } else {

                    }
                }

            })

        Handler().postDelayed({
            navController.navigate(R.id.loading_to_signin)
        }, 3000)
    }
}
