package com.example.helper.ui.history

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import android.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.helper.R
import com.example.helper.adapter.HistoryAdapter
import com.example.helper.adapter.SwipeCallBack
import com.example.helper.api.RetrofitClient
import com.example.helper.model.Books
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_history.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryFragment : Fragment() {

    private var listBooks: ArrayList<Books> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_history, container, false)

        val abh = root.findViewById<Toolbar>(R.id.abh)

        abh.inflateMenu(R.menu.items_history)

        abh.setOnMenuItemClickListener { onOptionsItemSelected(item = it) }

        val btn = abh.findViewById<Button>(R.id.menuBackH)

        btn.setOnClickListener {
            requireActivity().findViewById<DrawerLayout>(R.id.drawer_layout)
                .openDrawer(GravityCompat.START)
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshHis.setOnRefreshListener {
            refreshHis.isRefreshing = true
            populateBooks()
            refreshHis.isRefreshing = false
        }

        recyclerHistory.addItemDecoration(
            DividerItemDecoration(
                requireActivity(),
                DividerItemDecoration.VERTICAL
            )
        )

        populateBooks()

        val swipeHandler = object : SwipeCallBack(requireActivity()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerHistory.adapter as HistoryAdapter

                val position = viewHolder.adapterPosition

                if (direction == ItemTouchHelper.LEFT) {
                    val deletedModel = listBooks[position]
                    adapter.removeItem(position)

                    val snackbar = Snackbar.make(
                        requireActivity().window.decorView.rootView,
                        "${deletedModel.car} "+getString(R.string.removeItemMessage),
                        Snackbar.LENGTH_LONG
                    )
                    snackbar.setAction(getString(R.string.undoActionText)) {
                        adapter.restoreItem(deletedModel, position)
                    }
                    snackbar.setActionTextColor(Color.YELLOW)
                    snackbar.show()
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerHistory)

    }

    private fun populateBooks() {
        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.getAllBooks(token).enqueue(object : Callback<ArrayList<Books>> {
            override fun onFailure(call: Call<ArrayList<Books>>, t: Throwable) {
                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ArrayList<Books>>,
                response: Response<ArrayList<Books>>
            ) {
                if (response.isSuccessful) {
                    listBooks.clear()
                    recyclerHistory.adapter = HistoryAdapter(listBooks, token)
                    listBooks.addAll(response.body()!!)
                    recyclerHistory.layoutManager = LinearLayoutManager(requireActivity())
                    recyclerHistory.adapter = HistoryAdapter(listBooks, token)
                } else {
                    Toast.makeText(requireActivity(), "Something wrong", Toast.LENGTH_LONG).show()
                }
            }

        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.clearAll -> {
                val builder = AlertDialog.Builder(requireActivity())
                builder.setTitle(getString(R.string.infoAlertFav))
                    .setMessage(getString(R.string.messageAlertFav))
                    .setPositiveButton(getString(R.string.yesMessage)) { id, btn ->
                        deleteAllBook()
                    }
                    .setNegativeButton(getString(R.string.cancelMessage)) { id, btn ->

                    }

                val alert = builder.create()
                alert.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllBook() {
        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.removeAllBook(token).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    Toast.makeText(requireActivity(), "History clear!", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(requireActivity(), "Something wrong!", Toast.LENGTH_LONG).show()
                }
            }

        })
    }
}