package com.example.helper.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.helper.BottomSheet
import com.example.helper.R
import com.example.helper.api.RetrofitClient
import com.example.helper.model.Car
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.navigation.NavigationView
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.errors.ApiException
import com.google.maps.model.DirectionsResult
import com.google.maps.model.LatLng
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_map.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    /*TODO:
     * Выполнить работу над масштабированием
     * Доработать обработчик о запросе на определение местоположения
     */

    private var navController: NavController? = null

    private var googleMap: GoogleMap? = null

    private lateinit var lastLocation: Location

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var polyline: Polyline? = null

    private var width: Int? = null

    private var markerOld: Marker? = null

    @SuppressLint("SetTextI18n")
    override fun onMarkerClick(marker: Marker?): Boolean {

        //Click to show Bottom sheet
        val bottomSheet = BottomSheet()
        bottomSheet.showNow(this.childFragmentManager, "BottomSheet")

        //Way
        markerOld?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_yellow))

        val geoApiContext = GeoApiContext.Builder()
            .apiKey(resources.getString(R.string.google_maps_key))
            .build()

        var result: DirectionsResult? = null
        try {
            result = DirectionsApi.newRequest(geoApiContext)
                .origin(LatLng(lastLocation.latitude, lastLocation.longitude))
                .destination(LatLng(marker!!.position.latitude, marker.position.longitude)).await()
        } catch (e: ApiException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        polyline?.remove()

        marker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_purple))

        markerOld = marker

        val path: List<LatLng> = result!!.routes[0].overviewPolyline.decodePath()

        val polyOp = PolylineOptions()

        bottomSheet.carInfo.text = marker!!.title

        val resultDistance = FloatArray(10)
        Location.distanceBetween(
            lastLocation.latitude, lastLocation.longitude,
            marker.position.latitude, marker.position.longitude, resultDistance
        ).toString()

        bottomSheet.wayInfo.text = (resultDistance[0].toString().split('.')[0] + " m")
        bottomSheet.minuteBtn.setOnClickListener {
            if (bottomSheet.time.text == "Time:2880" || bottomSheet.time.text.toString().replace(
                    "Time:",
                    ""
                ).toInt() > 2880
            ) {
                Toast.makeText(
                    requireContext(),
                    "Привышено ограничение в 2880 минуты!",
                    Toast.LENGTH_LONG
                ).show()
                bottomSheet.time.text = "Time:2880"
            } else {
                if (bottomSheet.price.text == "Price:0" && bottomSheet.time.text == "Time:0") {
                    bottomSheet.price.text = "Price:60"
                    bottomSheet.time.text = "Time:1"
                } else {
                    val valueTime = bottomSheet.time.text.toString().replace("Time:", "")
                    val sumTime = valueTime.toInt() + 1
                    bottomSheet.time.text = "Time:$sumTime"

                    val valuePrice = bottomSheet.price.text.toString().replace("Price:", "")
                    val sumPrice = valuePrice.toInt() + 60
                    bottomSheet.price.text = "Price:$sumPrice"
                }
            }
        }
        bottomSheet.hourBtn.setOnClickListener {
            if (bottomSheet.time.text == "Time:2880" || bottomSheet.time.text.toString().replace(
                    "Time:",
                    ""
                ).toInt() > 2880
            ) {
                Toast.makeText(
                    requireContext(),
                    "Привышено ограничение в 2880 минуты!",
                    Toast.LENGTH_LONG
                ).show()
                bottomSheet.time.text = "Time:2880"
            } else {
                if (bottomSheet.price.text == "Price:0" && bottomSheet.time.text == "Time:0") {
                    bottomSheet.price.text = "Price:3000"
                    bottomSheet.time.text = "Time:60"
                } else {
                    val valueTime = bottomSheet.time.text.toString().replace("Time:", "")
                    val sumTime = valueTime.toInt() + 60
                    bottomSheet.time.text = "Time:$sumTime"

                    val valuePrice = bottomSheet.price.text.toString().replace("Price:", "")
                    val sumPrice = valuePrice.toInt() + 3000
                    bottomSheet.price.text = "Price:$sumPrice"
                }
            }
        }
        bottomSheet.dayBtn.setOnClickListener {
            if (bottomSheet.time.text == "Time:2880" || bottomSheet.time.text.toString().replace(
                    "Time:",
                    ""
                ).toInt() > 2880
            ) {
                Toast.makeText(
                    requireContext(),
                    "Привышено ограничение в 2880 минуты!",
                    Toast.LENGTH_LONG
                ).show()
                bottomSheet.time.text = "Time:2880"
            } else {
                if (bottomSheet.price.text == "Price:0" && bottomSheet.time.text == "Time:0") {
                    bottomSheet.price.text = "Price:18000"
                    bottomSheet.time.text = "Time:1440"
                } else {
                    val valueTime = bottomSheet.time.text.toString().replace("Time:", "")
                    val sumTime = valueTime.toInt() + 1440
                    bottomSheet.time.text = "Time:$sumTime"

                    val valuePrice = bottomSheet.price.text.toString().replace("Price:", "")
                    val sumPrice = valuePrice.toInt() + 18000
                    bottomSheet.price.text = "Price:$sumPrice"
                }
            }
        }
        bottomSheet.bookBtn.setOnClickListener {
            if (bottomSheet.time.text.toString().replace("Time:", "").toInt() > 0) {
                val bundle = bundleOf(
                    "wait" to bottomSheet.time.text.toString().replace("Time:", ""),
                    "car" to bottomSheet.carInfo.text.toString(),
                    "price" to bottomSheet.price.text.toString().replace("Price:", "")
                )
                navController!!.navigate(R.id.map_to_timer, bundle)
            } else {
                Toast.makeText(requireContext(), "Timer is 0!", Toast.LENGTH_LONG).show()
            }

        }

        val latBuilder: LatLngBounds.Builder = LatLngBounds.builder()
        for (item in path) {
            polyOp.add(com.google.android.gms.maps.model.LatLng(item.lat, item.lng))
            latBuilder.include(com.google.android.gms.maps.model.LatLng(item.lat, item.lng))
        }
        polyOp.width(16f)?.color(R.color.borderAuth)
        polyline = googleMap!!.addPolyline(polyOp)
        val latLngBounds: LatLngBounds = latBuilder.build()
        val update: CameraUpdate =
            CameraUpdateFactory.newLatLngBounds(latLngBounds, width!!, width!!, 25)
        googleMap!!.animateCamera(update)

        return true
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this.requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101
            )
            return
        }


        //Current location
        this.googleMap?.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this.requireActivity()) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng =
                    com.google.android.gms.maps.model.LatLng(location.latitude, location.longitude)
                this.googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 20f))
                view().findViewById<TextView>(R.id.palaceTxt).text = getAddress(currentLatLng)
            }
        }

        //Cars getting
        RetrofitClient.instance.getCars()
            .enqueue(object : Callback<ArrayList<Car>> {
                override fun onFailure(call: Call<ArrayList<Car>>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<ArrayList<Car>>,
                    response: Response<ArrayList<Car>>
                ) {
                    if (response.isSuccessful) {
                        for (item in response.body()!!) {
                            val carOnMap =
                                com.google.android.gms.maps.model.LatLng(
                                    item.longitude.toDouble(),
                                    item.latitude.toDouble()
                                )
                            googleMap?.addMarker(
                                MarkerOptions().position(carOnMap).title(item.mark).icon(
                                    BitmapDescriptorFactory.fromResource(R.drawable.car_yellow)
                                )
                            )
                        }
                    } else {
                        Toast.makeText(context, "No", Toast.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun getAddress(latLng: com.google.android.gms.maps.model.LatLng): String {
        val geo = Geocoder(this.requireActivity(), Locale.getDefault())
        val address = geo.getFromLocation(latLng.latitude, latLng.longitude, 1)
        return address[0].locality
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        this.googleMap!!.uiSettings.isZoomControlsEnabled = true
        setUpMap()
        this.googleMap?.setOnMarkerClickListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        navController = Navigation.findNavController(view)

        openDrawer.setOnClickListener {
            val drawer = requireActivity().findViewById<DrawerLayout>(R.id.drawer_layout)
            drawer.openDrawer(GravityCompat.START)
        }
    }

    private fun view(): View {
        val nv = this.requireActivity().findViewById<NavigationView>(R.id.nav_view_header)
        return nv.getHeaderView(0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(this.requireActivity())

        width = resources.displayMetrics.widthPixels

        return inflater.inflate(R.layout.fragment_map, container, false)
    }

}