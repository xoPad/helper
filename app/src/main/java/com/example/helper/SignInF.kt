package com.example.helper


import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.helper.api.RetrofitClient
import com.example.helper.model.LoginResponse
import com.example.helper.model.User
import kotlinx.android.synthetic.main.fragment_sign_in.*
import retrofit2.*
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo


/**
 * A simple [Fragment] subclass.
 */
class SignInF : Fragment(), OnClickListener {

    /*TODO
     * Сохранение значение в полях ввода (в регистрации также)
     * Выполнить при авторизованности переход к картам => проверка валидности токена забирая его из файла (локальное хранение)
     */

    private var mySettings:SharedPreferences? = null

    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mySettings = requireActivity().getSharedPreferences("Account", MODE_PRIVATE)
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        view.findViewById<TextView>(R.id.newAccountCreate).setOnClickListener(this)
        view.findViewById<TextView>(R.id.forgotTV).setOnClickListener(this)
        view.findViewById<Button>(R.id.signinBtn).setOnClickListener(this)
    }

    fun authorizationMethod(){
        val loginTxt = login.text.toString().trim()
        val passwordTxt = password.text.toString().trim()
        val user = User(loginTxt, passwordTxt)

        if(loginTxt.isEmpty()){
            login.error = requireContext().resources.getString(R.string.error_login)
            login.requestFocus()
            return
        }

        if(passwordTxt.isEmpty()){
            password.error = requireContext().resources.getString(R.string.error_password)
            password.requestFocus()
            return
        }

        RetrofitClient.instance.userLogin(user)
            .enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (response.isSuccessful) {
                        //Сохранение токена для прохождения в следующее активити без авторизации
                        val myEditor = mySettings!!.edit()
                        myEditor.putString("TokenAccess", response.body()?.accessToken)
                        myEditor.putString("TokenRefresh", response.body()?.refreshToken)
                        myEditor.apply()

                        //Передача в следующее активити значения
                        val myIntent = Intent(activity, NavigationActivity::class.java)
                        myIntent.putExtra("token", response.body()?.accessToken)
                        myIntent.putExtra("refresh", response.body()?.refreshToken)
                        activity!!.startActivity(myIntent)
                    } else {
                        Toast.makeText(context, "No", Toast.LENGTH_LONG).show()
                    }
                }

            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.newAccountCreate -> {
                navController!!.navigate(R.id.signin_to_signup)
            }
            R.id.forgotTV -> navController!!.navigate(R.id.signin_to_signup)
            R.id.signinBtn -> {
                authorizationMethod()
            }
        }
    }
}
