package com.example.carsharing


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.carsharing.api.RetrofitClient
import com.example.carsharing.model.Registration
import com.example.carsharing.model.ResponseMessage
import kotlinx.android.synthetic.main.fragment_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class SignUpF : Fragment(), View.OnClickListener {

    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        view.findViewById<Button>(R.id.signupBtn).setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.signupBtn -> {
                val loginTxt = loginReg.text.toString().trim()
                val emailTxt = emailReg.text.toString().trim()
                val passwordFirst = passwordReg.text.toString().trim()
                val passwordSecond = passwordRegMore.text.toString().trim()

                if(loginTxt.isEmpty()){
                    loginReg.error = requireContext().resources.getString(R.string.error_name)
                    loginReg.requestFocus()
                    return
                }

                if(emailTxt.isEmpty()){
                    emailReg.error = requireContext().resources.getString(R.string.error_login)
                    emailReg.requestFocus()
                    return
                }

                if(passwordFirst.isEmpty()){
                    passwordReg.error = requireContext().resources.getString(R.string.error_password)
                    passwordReg.requestFocus()
                    return
                }

                if(passwordSecond.isEmpty()){
                    passwordRegMore.error = requireContext().resources.getString(R.string.error_password)
                    passwordRegMore.requestFocus()
                    return
                }

                val newUser = Registration(loginTxt, emailTxt, passwordSecond, "Null")

                if (passwordFirst == passwordSecond) {
                    RetrofitClient.instance.createUser(newUser)
                        .enqueue(object : Callback<ResponseMessage> {
                            override fun onFailure(call: Call<ResponseMessage?>, t: Throwable) {
                                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                            }

                            override fun onResponse(
                                call: Call<ResponseMessage?>,
                                response: Response<ResponseMessage?>
                            ) {
                                if(response.isSuccessful){
                                    navController!!.navigate(R.id.back_to_signin)
                                }else{
                                    Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show()
                                }
                            }

                        })
                } else Toast.makeText(context, "Passwords are different!", Toast.LENGTH_LONG).show()
            }
        }
    }
}
