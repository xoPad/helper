package com.example.helper

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.helper.api.RetrofitClient
import kotlinx.android.synthetic.main.activity_navigation.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity() {

    private var mySettings: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        mySettings = getSharedPreferences("Lang", Context.MODE_PRIVATE)
        val lang = mySettings!!.getString("lang", "ru")
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val conf = Configuration()
        conf.setLocale(locale)
        resources.updateConfiguration(conf, resources.displayMetrics)

        setContentView(R.layout.activity_main)


    }
}
